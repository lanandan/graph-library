package com.example.graphlibrary;

import java.util.ArrayList;
import java.util.Calendar;
import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import android.support.v7.app.ActionBarActivity;
import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class GraphInput_Activity extends ActionBarActivity implements OnClickListener{
	Button save_and_add,next;
	EditText edttamil,edtenglish,edtmaths,edtscience,edtsocial;
	TextView month;	
	public static ArrayList<String> marks;
	public static ArrayList<String> lstmonth;
	Spinner chart_type; 
	String[] str_chart_type={"Chart Type","Bar Chart","Line Chart","Pie Chart","Combined Chart"};
	
	ArrayAdapter<String> adapter_chart_type;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_graph_input);
		getSupportActionBar().hide();
		
		//Array List to Store data to send to next Class to draw Graph
		marks=new ArrayList<String>();
		lstmonth=new ArrayList<String>();
		
		//Button to Save and add
		save_and_add=(Button)findViewById(R.id.btnsaveandadd);
		save_and_add.setOnClickListener(this);
		
		
		
		//to get Month Picker
		month=(TextView)findViewById(R.id.txtmonth);
		month.setOnClickListener(this);
		
		//Text Field or Edit Text to get Mark
		edttamil=(EditText)findViewById(R.id.edttamil);
		edtenglish=(EditText)findViewById(R.id.edtenglish);
		edtmaths=(EditText)findViewById(R.id.edtmaths);
		edtscience=(EditText)findViewById(R.id.edtscience);
		edtsocial=(EditText)findViewById(R.id.edtsocial);
		
		
		
		
		 edttamil.setOnFocusChangeListener(new OnFocusChangeListener() {          

		        public void onFocusChange(View v, boolean hasFocus) {
		            if(hasFocus)
		           {
		            	if(month.getText().equals("Month"))
		    			{
		    				edttamil.setText("");
		    				edttamil.setError("Fill the Month");
		    			}
		           }
		        }
		    });
		
		 edtenglish.setOnFocusChangeListener(new OnFocusChangeListener() {          

		        public void onFocusChange(View v, boolean hasFocus) {
		            if(hasFocus)
		           {
		            	if(month.getText().equals("Month"))
		    			{
		    				edtenglish.setText("");
		    				edtenglish.setError("Fill the Month");
		    			}
		           }
		        }
		    });
		
		 
		 edtmaths.setOnFocusChangeListener(new OnFocusChangeListener() {          

		        public void onFocusChange(View v, boolean hasFocus) {
		            if(hasFocus)
		           {
		            	if(month.getText().equals("Month"))
		    			{
		    				edtmaths.setText("");
		    				edtmaths.setError("Fill the Month");
		    			}
		           }
		        }
		    });
		
		 edtscience.setOnFocusChangeListener(new OnFocusChangeListener() {          

		        public void onFocusChange(View v, boolean hasFocus) {
		            if(hasFocus)
		           {
		            	if(month.getText().equals("Month"))
		    			{
		    				edtscience.setText("");
		    				edtscience.setError("Fill the Month");
		    			}
		           }
		        }
		    });
		
		 edtsocial.setOnFocusChangeListener(new OnFocusChangeListener() {          

		        public void onFocusChange(View v, boolean hasFocus) {
		            if(hasFocus)
		           {
		            	if(month.getText().equals("Month"))
		    			{
		    				edtsocial.setText("");
		    				edtsocial.setError("Fill the Month");
		    			}
		           }
		        }
		    });

		 
		   //  Spinner 
		 chart_type=(Spinner)findViewById(R.id.chart_type);
		 adapter_chart_type = new ArrayAdapter(this,android.R.layout.simple_spinner_item,str_chart_type);
		 adapter_chart_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 chart_type.setAdapter(adapter_chart_type);
		 
		 
		 
		 
		 chart_type.setOnItemSelectedListener(new OnItemSelectedListener() {
			    @Override
			    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
			        // your code here
			    if(position==1)
			    {
			    	openChart("Bar Chart");
			    }
			    if(position==2)
			    {
			    	openChart("Line Chart");
			    }
			    if(position==3)
			    {
			    	openChart("Pie Chart");
			    }
			    if(position==4)
			    {   	
			    	openChart("Combined Chart");
			    }
			    
			    }

			    @Override
			    public void onNothingSelected(AdapterView<?> parentView) {
			        // your code here
			    }

			});
		 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.btnsaveandadd)
		{
			if(edttamil.getText().equals(""))
			{
				marks.add(0+"");
			}
			else
			{
				marks.add(edttamil.getText().toString());
			}
			if(edtenglish.getText().equals(""))
			{
				marks.add(0+"");
			}
			else
			{
				marks.add(edtenglish.getText().toString());
			}
			if(edtmaths.getText().equals(""))
			{
				marks.add(0+"");
			}
			else
			{
				marks.add(edtmaths.getText().toString());
			}
			if(edtscience.getText().equals(""))
			{
				marks.add(0+"");
			}
			else
			{
				marks.add(edtscience.getText().toString());
			}
			if(edtsocial.getText().equals(""))
			{
				marks.add(0+"");
			}
			else
			{
				marks.add(edtsocial.getText().toString());
			}
			
			if(!lstmonth.contains(month.getText().toString()))
			{
				lstmonth.add(month.getText().toString());	
				edttamil.setText("");
				edtenglish.setText("");
				edtmaths.setText("");
				edtscience.setText("");
				edtsocial.setText("");
				Toast.makeText(this,"Your Marks where Added Successfully",Toast.LENGTH_LONG).show();
			}
			else
			{
				Toast.makeText(this,"That Month is already Added",Toast.LENGTH_LONG).show();
			}
		}
		
		if(v.getId()==R.id.txtmonth)
		{
			
			final Calendar c = Calendar.getInstance();
	         int Year = c.get(Calendar.YEAR);
	         int Month = c.get(Calendar.MONTH);
	         int Day = c.get(Calendar.DAY_OF_MONTH);        
	         DatePickerDialog dpd1 = new DatePickerDialog(this,
	                 new DatePickerDialog.OnDateSetListener() {	                     
	                     public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {
	                        int strtmonth=monthOfYear+1;
	                     	String Month[]={"Jan","Feb","March","April","May","June","July","Aug","Sep","Oct","Nov","Dec"};
	                        month.setText(Month[strtmonth-1]);
	                      }
	                     }, Year, Month, Day);	        
	         ((ViewGroup) dpd1.getDatePicker()).findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
	         ((ViewGroup) dpd1.getDatePicker()).findViewById(Resources.getSystem().getIdentifier("year", "id", "android")).setVisibility(View.GONE);
	         dpd1.show();			    
			
		}	
		

		
	}	

		
	 private void openChart(String chart_type)
	 {
		 
		 if(chart_type.equals("Bar Chart")||chart_type.equals("Line Chart"))
		 {	 
		 
		 if(lstmonth.size()==0)
		 {
			 Toast.makeText(this,"Sorry Not Possible to draw Chart",Toast.LENGTH_LONG).show();
		 }
		 if(lstmonth.size()>0)
		 {
			 XYSeries tamilSeries = new XYSeries("Tamil");		
			 XYSeries englishSeries = new XYSeries("English");
			 XYSeries mathsSeries = new XYSeries("Maths");
			 XYSeries scienceSeries = new XYSeries("Science");
			 XYSeries socialSeries = new XYSeries("Social");
					
			 
			 	 for(int i=0;i<lstmonth.size();i++){				 
			     tamilSeries.add(i+1,Integer.parseInt(marks.get(i)));
			     if(marks.size()>=i+1)
			     {
				 englishSeries.add(i+1,Integer.parseInt(marks.get(i+1)));
			     }
			     if(marks.size()>=i+2)
			     {
				 mathsSeries.add(i+1,Integer.parseInt(marks.get(i+2)));
			     }
			     if(marks.size()>=i+3)
			     {
				 scienceSeries.add(i+1,Integer.parseInt(marks.get(i+3)));
			     }
			     if(marks.size()>=i+4)
			     {
				  socialSeries.add(i+1,Integer.parseInt(marks.get(i+4)));
				     if(i+4==marks.size()-1)
			    	 {
			    		 break;
			    	 }
			     }
			 	 }
	     	
			 	    // Creating a dataset to hold each series
			        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			        dataset.addSeries(tamilSeries);
			        dataset.addSeries(englishSeries);
			        dataset.addSeries(mathsSeries);
			        dataset.addSeries(scienceSeries);
			        dataset.addSeries(socialSeries);
			        
			        // Creating XYSeriesRenderer to customize TamilSeries
			        XYSeriesRenderer tamilRenderer = new XYSeriesRenderer();
			        tamilRenderer.setColor(Color.WHITE);
			        tamilRenderer.setPointStyle(PointStyle.CIRCLE);
			        tamilRenderer.setFillPoints(true);
			        tamilRenderer.setLineWidth(2);
			        tamilRenderer.setDisplayChartValues(true);
			 
			        // Creating XYSeriesRenderer to customize EnglishSeries
			        XYSeriesRenderer englishRenderer = new XYSeriesRenderer();
			        englishRenderer.setColor(Color.YELLOW);
			        englishRenderer.setPointStyle(PointStyle.CIRCLE);
			        englishRenderer.setFillPoints(true);
			        englishRenderer.setLineWidth(2);
			        englishRenderer.setDisplayChartValues(true);
			        
			        // Creating XYSeriesRenderer to customize EnglishSeries
			        XYSeriesRenderer mathsRenderer = new XYSeriesRenderer();
			        mathsRenderer.setColor(Color.RED);
			        mathsRenderer.setPointStyle(PointStyle.CIRCLE);
			        mathsRenderer.setFillPoints(true);
			        mathsRenderer.setLineWidth(2);
			        mathsRenderer.setDisplayChartValues(true);

			        
			        // Creating XYSeriesRenderer to customize EnglishSeries
			        XYSeriesRenderer scienceRenderer = new XYSeriesRenderer();
			        scienceRenderer.setColor(Color.GREEN);
			        scienceRenderer.setPointStyle(PointStyle.CIRCLE);
			        scienceRenderer.setFillPoints(true);
			        scienceRenderer.setLineWidth(2);
			        scienceRenderer.setDisplayChartValues(true);
			        
			        // Creating XYSeriesRenderer to customize EnglishSeries
			        XYSeriesRenderer socialRenderer = new XYSeriesRenderer();
			        socialRenderer.setColor(Color.BLUE);
			        socialRenderer.setPointStyle(PointStyle.CIRCLE);
			        socialRenderer.setFillPoints(true);
			        socialRenderer.setLineWidth(2);
			        socialRenderer.setDisplayChartValues(true);
			 	 
			        
         		    // Creating a XYMultipleSeriesRenderer to customize the whole chart
			        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			        multiRenderer.setXLabels(0);
			        multiRenderer.setChartTitle("Analysis of Mark");
			        multiRenderer.setXTitle("Year 2015");
			        multiRenderer.setYTitle("Marks");
			        multiRenderer.setZoomButtonsVisible(true);
			        
			        for(int i=0;i<lstmonth.size();i++){
			            multiRenderer.addXTextLabel(i+1,lstmonth.get(i));
			        }
			        
			        multiRenderer.addSeriesRenderer(tamilRenderer);
			        multiRenderer.addSeriesRenderer(englishRenderer);
			        multiRenderer.addSeriesRenderer(mathsRenderer);
			        multiRenderer.addSeriesRenderer(scienceRenderer);
			        multiRenderer.addSeriesRenderer(socialRenderer);
			        
			        Intent intent;			        
			        if(chart_type.equals("Bar Chart"))
			        {
			        	// Creating an intent to plot line chart using dataset and multipleRenderer
				        intent = ChartFactory.getBarChartIntent(getBaseContext(), dataset, multiRenderer,org.achartengine.chart.BarChart.Type.DEFAULT);
				        // Start Activity
				        startActivity(intent);	
			        }			        
			        if(chart_type.equals("Line Chart"))
			        {
			        	intent = ChartFactory.getLineChartIntent(getBaseContext(), dataset, multiRenderer);
			        	 // Start Activity
				        startActivity(intent);       
			        }			        
			        	 
		 }      
	    }	
		 
		 else
		 {
			
			 openChart();
			 
		 }
		 
		 
	 }
	 private void openChart() {

		 
		 
		
		String[] code = new String[] { "Tamil","English","Maths","Science","Social" };

		double tamil = 0,english = 0,maths = 0,science = 0,social = 0;
		
		
		
		     for(int i=0;i<lstmonth.size();i++)
		     {
		    	tamil=tamil+Integer.parseInt(marks.get(i));		     	
		     if(marks.size()>=i+1)
		     {
		    	 
		    	 english=english+Integer.parseInt(marks.get(i+1));
			 
		     }
		     if(marks.size()>=i+2)
		     {
		    	 maths=maths+Integer.parseInt(marks.get(i+2));			 
		     }
		     if(marks.size()>=i+3)
		     {
		    	 science=science+Integer.parseInt(marks.get(i+3));
			 
		     }
		     if(marks.size()>=i+4)
		     {
		    	 social=social+Integer.parseInt(marks.get(i+4));	
		    	 if(i+4==marks.size()-1)
		    	 {
		    		 break;
		    	 }	    	 
		    	 
		     }
		 	 }
		
		
		
		
		// Pie Chart Section Value
		double[] distribution = {tamil,english,maths,science,social};

		// Color of each Pie Chart Sections
		int[] colors = { Color.BLUE, Color.MAGENTA, Color.GREEN, Color.CYAN,Color.RED };
		
		// Instantiating CategorySeries to plot Pie Chart
		CategorySeries distributionSeries = new CategorySeries("Android version distribution as on October 1, 2012");
		for (int i = 0; i < distribution.length; i++) {
		// Adding a slice with its values and name to the Pie Chart
		distributionSeries.add(code[i], distribution[i]);
		}

		// Instantiating a renderer for the Pie Chart
		DefaultRenderer defaultRenderer = new DefaultRenderer();
		for (int i = 0; i < distribution.length; i++) {
		SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
		seriesRenderer.setColor(colors[i]);
		seriesRenderer.setDisplayChartValues(true);
		//Adding colors to the chart
		defaultRenderer.setBackgroundColor(Color.BLACK);
		defaultRenderer.setApplyBackgroundColor(true);
		// Adding a renderer for a slice
		defaultRenderer.addSeriesRenderer(seriesRenderer);
		}

		defaultRenderer.setChartTitle("Android version distribution as on December 1, 2014. ");
		defaultRenderer.setChartTitleTextSize(20);
		defaultRenderer.setZoomButtonsVisible(false);
       Intent intent = ChartFactory.getPieChartIntent(getBaseContext(),distributionSeries , defaultRenderer, "AChartEnginePieChartDemo");

       startActivity(intent);
       

		}

	 
}


























